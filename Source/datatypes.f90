!=== COPYRIGHT AND LICENSE STATEMENT ===
!
!  This file is part of the TensorProductMultigrid code.
!  
!  (c) The copyright relating to this work is owned jointly by the
!  Crown, Met Office and NERC [2014]. However, it has been created
!  with the help of the GungHo Consortium, whose members are identified
!  at https://puma.nerc.ac.uk/trac/GungHo/wiki .
!  
!  Main Developer: Eike Mueller
!  
!  TensorProductMultigrid is free software: you can redistribute it and/or
!  modify it under the terms of the GNU Lesser General Public License as
!  published by the Free Software Foundation, either version 3 of the
!  License, or (at your option) any later version.
!  
!  TensorProductMultigrid is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU Lesser General Public License for more details.
!  
!  You should have received a copy of the GNU Lesser General Public License
!  along with TensorProductMultigrid (see files COPYING and COPYING.LESSER).
!  If not, see <http://www.gnu.org/licenses/>.
!
!=== COPYRIGHT AND LICENSE STATEMENT ===


!==================================================================
!
!   Grid data types for three dimensional cell centred grids.
!   We always assume that the number of gridcells and size in
!   the x- and y- direction is identical.
!
!    Eike Mueller, University of Bath, Feb 2012
!
!==================================================================


module datatypes

  use mpi
  use parameters
  use messages

  implicit none

! Vertical boundary conditions
  integer, parameter :: VERTBC_DIRICHLET = 1
  integer, parameter :: VERTBC_NEUMANN = 2

! Parameters of three dimensional grid
  type grid_parameters
    integer :: n         ! Total number of grid cells in horizontal direction
    integer :: nz        ! Total number of grid cells in vertical direction
    real(kind=rl) :: L   ! Global extent of grid in horizontal direction
    real(kind=rl) :: H   ! Global extent of grid in vertical direction
    integer :: vertbc    ! Vertical boundary condition (see VERTBC_DIRICHLET
                         ! and VERTBC_NEUMANN)
    logical :: graded    ! Is the vertical grid graded?
  end type grid_parameters

! Three dimensional scalar field s(z,y,x)
  type scalar3d
    integer :: ix_min     ! } (Inclusive) range of locally owned cells
    integer :: ix_max     ! } in the x-direction
    integer :: iy_min     ! } (these ranges DO NOT include halo cells)
    integer :: iy_max     ! } in the y-direction
    integer :: icompx_min ! } (Inclusive) ranges of computational cells,
    integer :: icompx_max ! } in local coords. All cells in these ranges
    integer :: icompy_min ! } are included in calculations, e.g. in the
    integer :: icompy_max ! } smoother. This allows duplicating operations
                          ! } on part of the halo for RB Gauss Seidel
    integer :: halo_size  ! Size of halos
    logical :: isactive   ! Is this field active, i.e. used on one of the
                          ! active processes on coarser grids?
    real(kind=rl),allocatable :: s(:,:,:)
    type(grid_parameters) :: grid_param
  end type scalar3d

public::VERTBC_DIRICHLET
public::VERTBC_NEUMANN
public::scalar3d
public::grid_parameters
public::L2norm
public::daxpy_scalar3d
public::save_scalar3d
public::create_scalar3d
public::volscale_scalar3d
public::destroy_scalar3d
public::volume_of_element
public::r_grid

private

  ! Vertical grid, this array of length n_z+1 stores the
  ! vertices of the grid in the vertical direction
  real(kind=rl), allocatable :: r_grid(:)

  contains

!==================================================================
! volume of element on cubed sphere grid
! NB: ix,iy are global indices
!==================================================================
  real(kind=rl) function volume_of_element(ix,iy,grid_param)
    implicit none
    integer, intent(in) :: ix
    integer, intent(in) :: iy
    type(grid_parameters), intent(in) :: grid_param
    real(kind=rl) :: h
    real(kind=rl) :: rho_i, sigma_j
    h = 2.0_rl/grid_param%n
    rho_i = 2.0_rl*(ix-0.5_rl)/grid_param%n-1.0_rl
    sigma_j = 2.0_rl*(iy-0.5_rl)/grid_param%n-1.0_rl
    volume_of_element = (1.0_rl+rho_i**2+sigma_j**2)**(-1.5_rl)*h**2
  end function volume_of_element

!==================================================================
! Create scalar3d field on fine grid and set to zero
!==================================================================
  subroutine create_scalar3d(comm_horiz,grid_param, halo_size, phi)
    implicit none

    integer                             :: comm_horiz  ! Horizontal communicator
    type(grid_parameters), intent(in)   :: grid_param  ! Grid parameters
    integer, intent(in)                 :: halo_size   ! Halo size
    type(scalar3d), intent(inout)       :: phi         ! Field to create
    integer                             :: nproc       ! Number of processes
    integer                             :: rank, ierr  ! rank and MPI error
    integer, dimension(2)               :: p_horiz     ! position in 2d
                                                       ! processor grid
    integer                             :: nlocal      ! Local number of
                                                       ! cells in horizontal
                                                       ! direction
    integer, parameter                  :: dim_horiz = 2 ! horiz. dimension

    phi%grid_param = grid_param
    call mpi_comm_size(comm_horiz, nproc, ierr)
    nlocal = grid_param%n/sqrt(1.0*nproc)

    ! Work out position in 2d processor grid
    call mpi_comm_rank(comm_horiz, rank, ierr)
    call mpi_cart_coords(comm_horiz,rank,dim_horiz,p_horiz,ierr)
    ! Set local data ranges
    ! NB: p_horiz stores (py,px) in that order (see comment in
    ! communication module)
    phi%iy_min = p_horiz(1)*nlocal + 1
    phi%iy_max = (p_horiz(1)+1)*nlocal
    phi%ix_min = p_horiz(2)*nlocal + 1
    phi%ix_max = (p_horiz(2)+1)*nlocal
    ! Set computational ranges. Note that these are different at
    ! the edges of the domain!
    if (p_horiz(1) == 0) then
      phi%icompy_min = 1
    else
      phi%icompy_min = 1 - (halo_size - 1)
    end if
    if (p_horiz(1) == floor(sqrt(1.0_rl*nproc))-1) then
      phi%icompy_max = nlocal
    else
      phi%icompy_max = nlocal + (halo_size - 1)
    end if
    if (p_horiz(2) == 0) then
      phi%icompx_min = 1
    else
      phi%icompx_min = 1 - (halo_size - 1)
    end if
    if (p_horiz(2) == floor(sqrt(1.0_rl*nproc))-1) then
      phi%icompx_max = nlocal
    else
      phi%icompx_max = nlocal + (halo_size - 1)
    end if
    ! Set halo size
    phi%halo_size = halo_size
    ! Set field to active
    phi%isactive = .true.
    ! Allocate memory
    allocate(phi%s(0:grid_param%nz+1,            &
                   1-halo_size:nlocal+halo_size, &
                   1-halo_size:nlocal+halo_size))
    phi%s(:,:,:) = 0.0_rl

  end subroutine create_scalar3d

!==================================================================
! Destroy scalar3d field on fine grid
!==================================================================
  subroutine destroy_scalar3d(phi)
    implicit none
    type(scalar3d), intent(inout) :: phi

    deallocate(phi%s)

  end subroutine destroy_scalar3d

!==================================================================
! Scale fields with volume of element
! Either multiply with volume factor |T| v_k (power = 1)
! or divide by it (power = -1)
!==================================================================
  subroutine volscale_scalar3d(phi,power)
    implicit none
    type(scalar3d), intent(inout) :: phi
    integer, intent(in) :: power
    integer :: ix, iy, iz
    integer :: ierr
    integer :: nlocalx, nlocaly
    real(kind=rl) :: vol_h, vol_r, h, tmp

    if (.not. ( ( power .eq. 1) .or. (power .eq. -1) ) ) then
      call fatalerror("power has to be -1 or 1 when volume-scaling fields")
    end if

    nlocalx = phi%ix_max-phi%ix_min+1
    nlocaly = phi%iy_max-phi%iy_min+1

    if (phi%isactive) then
      do ix=1,nlocalx
        do iy=1,nlocaly
#ifdef CARTESIANGEOMETRY
          h = phi%grid_param%L/phi%grid_param%n
          vol_h = h**2
#else
          vol_h = volume_of_element(ix+(phi%ix_min-1), &
                                    iy+(phi%iy_min-1), &
                                    phi%grid_param)
#endif
          do iz=1,phi%grid_param%nz
#ifdef CARTESIANGEOMETRY
            vol_r = r_grid(iz+1)-r_grid(iz)
#else
            vol_r = (r_grid(iz+1)**3 - r_grid(iz)**3)/3.0_rl
#endif
            if (power == 1) then
              tmp = vol_h*vol_r
            else
              tmp = 1.0_rl/(vol_h*vol_r)
            end if
            phi%s(iz,iy,ix) = tmp*phi%s(iz,iy,ix)
          end do
        end do
      end do
    end if

  end subroutine volscale_scalar3d

!==================================================================
! Calculate L2 norm
! If phi_is_volumeintegral is .true. then phi is interpreted
! as the volume integral in a cell, otherwise it is interpreted as the
! average value in a cell.
!==================================================================
  real(kind=rl) function l2norm(phi,phi_is_volumeintegral)
    implicit none
    type(scalar3d), intent(in) :: phi
    logical, optional :: phi_is_volumeintegral
    integer :: ix, iy, iz
    real(kind=rl) :: tmp, global_tmp
    integer :: ierr
    integer :: nlocalx, nlocaly
    real(kind=rl) :: vol_h, vol_r, h
    logical :: divide_by_volume
    real(kind=rl) :: volume_factor
    if (present(phi_is_volumeintegral)) then
      divide_by_volume = phi_is_volumeintegral
    else
      divide_by_volume = .false.
    end if

    nlocalx = phi%ix_max-phi%ix_min+1
    nlocaly = phi%iy_max-phi%iy_min+1

    tmp = 0.0_rl
    if (phi%isactive) then
      do ix=1,nlocalx
        do iy=1,nlocaly
#ifdef CARTESIANGEOMETRY
          h = phi%grid_param%L/phi%grid_param%n
          vol_h = h**2
#else
          vol_h = volume_of_element(ix+(phi%ix_min-1), &
                                    iy+(phi%iy_min-1), &
                                    phi%grid_param)
#endif
          do iz=1,phi%grid_param%nz
#ifdef CARTESIANGEOMETRY
            vol_r = r_grid(iz+1)-r_grid(iz)
#else
            vol_r = (r_grid(iz+1)**3 - r_grid(iz)**3)/3.0_rl
#endif
            if (divide_by_volume) then
              volume_factor = 1.0_rl/(vol_h*vol_r)
            else
              volume_factor = vol_h*vol_r
            end if
            tmp = tmp + volume_factor*phi%s(iz,iy,ix)**2
          end do
        end do
      end do
    end if

    call mpi_allreduce(tmp,global_tmp, 1, &
                       MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,ierr)
    l2norm = dsqrt(global_tmp)
  end function l2norm

!==================================================================
! calculate phi <- phi + alpha*dphi
!==================================================================
  subroutine daxpy_scalar3d(alpha,dphi,phi)
    implicit none
    real(kind=rl), intent(in) :: alpha
    type(scalar3d), intent(in) :: dphi
    type(scalar3d), intent(inout) :: phi
    integer :: nlin
    integer :: nlocalx, nlocaly

    nlocalx = phi%ix_max-phi%ix_min+1
    nlocaly = phi%iy_max-phi%iy_min+1
    nlin = (nlocalx+2*phi%halo_size) &
         * (nlocaly+2*phi%halo_size) &
         * (phi%grid_param%nz+2)

    call daxpy(nlin,alpha,dphi%s,1,phi%s,1)

  end subroutine daxpy_scalar3d

!==================================================================
! Save scalar field to file
!==================================================================
  subroutine save_scalar3d(comm_horiz,phi,filename)
    implicit none
    integer, intent(in) :: comm_horiz
    type(scalar3d), intent(in) :: phi
    character(*), intent(in)   :: filename
    integer :: file_id = 100
    integer :: ix,iy,iz
    integer :: nlocal
    integer :: rank, nproc, ierr
    character(len=21) :: s

    nlocal = phi%ix_max-phi%ix_min+1

    ! Get number of processes and rank
    call mpi_comm_size(comm_horiz, nproc, ierr)
    call mpi_comm_rank(comm_horiz, rank, ierr)

    write(s,'(I10.10,"_",I10.10)') nproc, rank

    open(unit=file_id,file=trim(filename)//"_"//trim(s)//".dat")
    write(file_id,*) "# 3d scalar data file"
    write(file_id,*) "# ==================="
    write(file_id,*) "# Data is written as s(iz,iy,ix) "
    write(file_id,*) "# with the leftmost index running fastest"
    write(file_id,'(" n  = ",I8)') phi%grid_param%n
    write(file_id,'(" nz = ",I8)') phi%grid_param%nz
    write(file_id,'(" L  = ",F20.10)') phi%grid_param%L
    write(file_id,'(" H  = ",F20.10)') phi%grid_param%H
    write(file_id,'(" ix_min  = ",I10)') phi%ix_min
    write(file_id,'(" ix_max  = ",I10)') phi%ix_max
    write(file_id,'(" iy_min  = ",I10)') phi%iy_min
    write(file_id,'(" iy_max  = ",I10)') phi%iy_max
    write(file_id,'(" icompx_min  = ",I10)') phi%icompx_min
    write(file_id,'(" icompx_max  = ",I10)') phi%icompx_max
    write(file_id,'(" icompy_min  = ",I10)') phi%icompy_min
    write(file_id,'(" icompy_max  = ",I10)') phi%icompy_max
    write(file_id,'(" halosize    = ",I10)') phi%halo_size


    do ix=1-phi%halo_size,nlocal+phi%halo_size
      do iy=1-phi%halo_size,nlocal+phi%halo_size
        do iz=0,phi%grid_param%nz+1
          write(file_id,'(E24.15)') phi%s(iz,iy,ix)
        end do
      end do
    end do
    close(file_id)
  end subroutine save_scalar3d

end module datatypes
