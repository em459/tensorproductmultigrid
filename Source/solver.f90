!=== COPYRIGHT AND LICENSE STATEMENT ===
!
!  This file is part of the TensorProductMultigrid code.
!  
!  (c) The copyright relating to this work is owned jointly by the
!  Crown, Met Office and NERC [2014]. However, it has been created
!  with the help of the GungHo Consortium, whose members are identified
!  at https://puma.nerc.ac.uk/trac/GungHo/wiki .
!  
!  Main Developer: Eike Mueller
!  
!  TensorProductMultigrid is free software: you can redistribute it and/or
!  modify it under the terms of the GNU Lesser General Public License as
!  published by the Free Software Foundation, either version 3 of the
!  License, or (at your option) any later version.
!  
!  TensorProductMultigrid is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU Lesser General Public License for more details.
!  
!  You should have received a copy of the GNU Lesser General Public License
!  along with TensorProductMultigrid (see files COPYING and COPYING.LESSER).
!  If not, see <http://www.gnu.org/licenses/>.
!
!=== COPYRIGHT AND LICENSE STATEMENT ===


!==================================================================
!
! Solver parameters
!
!    Eike Mueller, University of Bath, May 2012
!
!==================================================================
module solver

  use parameters
  use datatypes

  implicit none

public::solver_parameters
public::SOLVER_RICHARDSON
public::SOLVER_CG

private

  integer, parameter :: SOLVER_RICHARDSON = 1
  integer, parameter :: SOLVER_CG = 2


  ! --- Solver parameters ---
  type solver_parameters
    integer :: solvertype ! Type of solver
    real(kind=rl) :: resreduction ! Required relative residual reduction
    integer :: maxiter    ! Maximal number of iterations
  end type solver_parameters

end module solver

