!=== COPYRIGHT AND LICENSE STATEMENT ===
!
!  This file is part of the TensorProductMultigrid code.
!  
!  (c) The copyright relating to this work is owned jointly by the
!  Crown, Met Office and NERC [2014]. However, it has been created
!  with the help of the GungHo Consortium, whose members are identified
!  at https://puma.nerc.ac.uk/trac/GungHo/wiki .
!  
!  Main Developer: Eike Mueller
!  
!  TensorProductMultigrid is free software: you can redistribute it and/or
!  modify it under the terms of the GNU Lesser General Public License as
!  published by the Free Software Foundation, either version 3 of the
!  License, or (at your option) any later version.
!  
!  TensorProductMultigrid is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU Lesser General Public License for more details.
!  
!  You should have received a copy of the GNU Lesser General Public License
!  along with TensorProductMultigrid (see files COPYING and COPYING.LESSER).
!  If not, see <http://www.gnu.org/licenses/>.
!
!=== COPYRIGHT AND LICENSE STATEMENT ===


!==================================================================
!
!  Main program for multigrid solver code for Helmholtz/Poisson
!  equation, discretised in the cell centred finite volume scheme
!
!    Eike Mueller, University of Bath, Feb 2012
!
!==================================================================

!==================================================================
! Main program
!==================================================================

program mg_main

  use discretisation
  use parameters
  use datatypes
  use multigrid
  use conjugategradient
  use solver
  use profiles
  use messages
  use communication
  use timer
  use mpi

  implicit none

  type(grid_parameters)     :: grid_param
  type(comm_parameters)     :: comm_param
  type(model_parameters)    :: model_param
  type(smoother_parameters) :: smoother_param
  type(mg_parameters)       :: mg_param
  type(cg_parameters)       :: cg_param
  type(solver_parameters)   :: solver_param

  type(scalar3d) :: u
  type(scalar3d) :: b
  type(scalar3d) :: r
#ifdef TESTCONVERGENCE
  type(scalar3d) :: uerror
  real(kind=rl) :: l2error
#endif

  ! Timers
  type(time) :: t_solve
  type(time) :: t_readparam
  type(time) :: t_initialise
  type(time) :: t_finalise

  ! --- Parameter file ---
  character(len=256) :: parameterfile

  ! --- Name of executable ---
  character(len=256) :: executable

  ! --- General parameters ---
  logical :: savefields   ! Save fields to disk?

  integer :: ierr

  integer :: x

  integer :: i, int_size

  x = 1
  do while(x == 0)
  end do

  ! Initialise MPI ...
  call mpi_init(ierr)

  ! ... and pre initialise communication module
  call comm_preinitialise()

  ! Parse command line arguments
  if (iargc() .lt. 2) then
    call getarg(0, executable)
    if (i_am_master_mpi) then
      write(STDOUT,*) "Usage: " // trim(executable) // " <parameterfile>"
    end if
    call mpi_finalize(ierr)
    stop
  end if

  call getarg(1, parameterfile)
  if (i_am_master_mpi) then
    write(STDOUT,*) "+--------------------------------------+"
    write(STDOUT,*) "+-- MULTIGRID SOLVER ------------------+"
    write(STDOUT,*) "+--------------------------------------+"
  end if

  if (i_am_master_mpi) then
    write(STDOUT,*) ''
    write(STDOUT,*) 'Compile time parameters:'
    write(STDOUT,*) ''
#ifdef CARTESIANGEOMETRY
    write(STDOUT,*) '  Geometry: Cartesian'
#else
    write(STDOUT,*) '  Geometry: Spherical'
#endif
#ifdef USELAPACK
    write(STDOUT,*) '  Use Lapack: Yes'
#else
    write(STDOUT,*) '  Use Lapack: No'
#endif
#ifdef OVERLAPCOMMS
    write(STDOUT,*) '  Overlap communications and calculation: Yes'
#else
    write(STDOUT,*) '  Overlap communications and calculation: No'
#endif
    write(STDOUT,*) ''
    i = huge(i)
    int_size = 1
    do while (i > 0)
      int_size = int_size + 1
      i = i/2
    end do
    write(STDOUT,'("   size of integer: ",I3," bit")') int_size
    write(STDOUT,*) ''
  end if

  ! Initialise timing module
  call initialise_timing("timing.txt")

  ! Read parameter files
  call initialise_timer(t_readparam,"t_readparam")
  call start_timer(t_readparam)
  if (i_am_master_mpi) then
    write(STDOUT,*) "Reading parameters from file '" // &
                    trim(parameterfile) // "'"
  end if
  call read_general_parameters(parameterfile,savefields)
  call read_solver_parameters(parameterfile,solver_param)
  call read_grid_parameters(parameterfile,grid_param)
  call read_comm_parameters(parameterfile,comm_param)
  call read_model_parameters(parameterfile,model_param)
  call read_smoother_parameters(parameterfile,smoother_param)
  call read_multigrid_parameters(parameterfile,mg_param)
  call read_conjugategradient_parameters(parameterfile,cg_param)
  call finish_timer(t_readparam)

  if (i_am_master_mpi) then
    write(STDOUT,*) ''
  end if

  ! Initialise discretisation module
  call discretisation_initialise(grid_param,     &
                                 model_param,    &
                                 smoother_param, &
                                 mg_param%n_lev )

  ! Initialise communication module
  call initialise_timer(t_initialise,"t_initialise")
  call start_timer(t_initialise)
  call comm_initialise(mg_param%n_lev,     &
                       mg_param%lev_split, &
                       grid_param,         &
                       comm_param)

  ! Initialise multigrid
  call mg_initialise(grid_param,       &
                     comm_param,       &
                     model_param,      &
                     smoother_param,   &
                     mg_param,         &
                     cg_param          &
                    )

  call create_scalar3d(MPI_COMM_HORIZ,grid_param,comm_param%halo_size,u)
  call create_scalar3d(MPI_COMM_HORIZ,grid_param,comm_param%halo_size,b)
  call create_scalar3d(MPI_COMM_HORIZ,grid_param,comm_param%halo_size,r)
  call initialise_rhs(grid_param,model_param,b)
#ifdef TESTCONVERGENCE
  call create_scalar3d(MPI_COMM_HORIZ,grid_param,comm_param%halo_size,uerror)
  call analytical_solution(grid_param,uerror)
#endif
  call finish_timer(t_initialise)
  if (i_am_master_mpi) then
    write(STDOUT,*) ''
  end if

  ! Initialise ghosts in initial solution, as mg_solve assumes that they
  ! are up-to-date
  call haloswap(mg_param%n_lev,pproc,u)

  ! Solve using multigrid
  call initialise_timer(t_solve,"t_solve")
  call start_timer(t_solve)
  comm_measuretime = .True.
#ifdef MEASUREHALOSWAP
  call measurehaloswap()
#else
  call mg_solve(b,u,solver_param)
#endif
  comm_measuretime = .False.
  call finish_timer(t_solve)

#ifdef TESTCONVERGENCE
  call daxpy_scalar3d(-1.0_rl,u,uerror)
  call haloswap(mg_param%n_lev,pproc,uerror)
  l2error = l2norm(uerror)
  if (i_am_master_mpi) then
    write(STDOUT,'("||error|| = ",E20.12," log_2(||error||) = ",E20.12)') &
      l2error, log(l2error)/log(2.0_rl)
  end if
  if (savefields) then
    call save_scalar3d(MPI_COMM_HORIZ,uerror,"error")
  end if
#endif

  ! Save fields to disk
  if (savefields) then
    call haloswap(mg_param%n_lev,pproc,u)
    call save_scalar3d(MPI_COMM_HORIZ,u,"solution")
    call volscale_scalar3d(b,1)
    call calculate_residual(mg_param%n_lev,pproc,b,u,r)
    call volscale_scalar3d(b,-1)
    call volscale_scalar3d(r,-1)
    call haloswap(mg_param%n_lev,pproc,r)
    call save_scalar3d(MPI_COMM_HORIZ,r,"residual")
  end if

  if (i_am_master_mpi) then
    write(STDOUT,*) ''
  end if

  call discretisation_finalise()

  ! Finalise
  call initialise_timer(t_finalise,"t_finalise")
  call start_timer(t_finalise)
  call mg_finalise()
  call cg_finalise()
  ! Deallocate memory
  call destroy_scalar3d(u)
  call destroy_scalar3d(b)
  call destroy_scalar3d(r)
#ifdef TESTCONVERGENCE
  call destroy_scalar3d(uerror)
#endif


  ! Finalise communications ...
  call comm_finalise(mg_param%n_lev,mg_param%lev_split)
  call finish_timer(t_finalise)
  call print_timerinfo("# --- Main timing results ---")
  call print_elapsed(t_readparam,.true.,1.0_rl)
  call print_elapsed(t_initialise,.true.,1.0_rl)
  call print_elapsed(t_solve,.true.,1.0_rl)
  call print_elapsed(t_finalise,.true.,1.0_rl)
  ! Finalise timing
  call finalise_timing()
  ! ... and MPI
  call mpi_finalize(ierr)

end program mg_main

!==================================================================
! Read general parameters from namelist file
!==================================================================
subroutine read_general_parameters(filename,savefields_out)
  use parameters
  use communication
  use messages
  use mpi
  implicit none
  character(*), intent(in) :: filename
  logical, intent(out) :: savefields_out
  integer, parameter :: file_id = 16
  logical :: savefields
  integer :: ierr
  namelist /parameters_general/ savefields
  if (i_am_master_mpi) then
    open(file_id,file=filename)
    read(file_id,NML=parameters_general)
    close(file_id)
    write(STDOUT,NML=parameters_general)
    write(STDOUT,'("---------------------------------------------- ")')
    write(STDOUT,'("General parameters")')
    write(STDOUT,'("    Save fields = ",L6)') savefields
    write(STDOUT,'("---------------------------------------------- ")')
    write(STDOUT,'("")')
  end if
  call mpi_bcast(savefields,1,MPI_LOGICAL,master_rank,MPI_COMM_WORLD,ierr)
  savefields_out = savefields
end subroutine read_general_parameters

!==================================================================
! Read solver parameters from namelist file
!==================================================================
subroutine read_solver_parameters(filename,solver_param_out)
  use solver
  use parameters
  use communication
  use messages
  use mpi
  implicit none
  character(*), intent(in) :: filename
  type(solver_parameters), intent(out) :: solver_param_out
  integer :: solvertype
  real(kind=rl) :: resreduction
  integer :: maxiter
  integer, parameter :: file_id = 16
  integer :: ierr
  namelist /parameters_solver/ solvertype,resreduction, maxiter
  if (i_am_master_mpi) then
    open(file_id,file=filename)
    read(file_id,NML=parameters_solver)
    close(file_id)
    write(STDOUT,NML=parameters_solver)
    write(STDOUT,'("---------------------------------------------- ")')
    write(STDOUT,'("Solver parameters ")')
    if (solvertype == SOLVER_RICHARDSON) then
      write(STDOUT,'("    solver       = Richardson")')
    else if (solvertype == SOLVER_CG) then
      write(STDOUT,'("    solver       = CG")')
    else
      call fatalerror("Unknown solver type")
    end if
    write(STDOUT,'("    maxiter      = ",I8)') maxiter
    write(STDOUT,'("    resreduction = ",E15.6)') resreduction
    write(STDOUT,'("---------------------------------------------- ")')
    write(*,'("")')
  end if
  call mpi_bcast(solvertype,1,MPI_INTEGER,master_rank,MPI_COMM_WORLD,ierr)
  call mpi_bcast(maxiter,1,MPI_INTEGER,master_rank,MPI_COMM_WORLD,ierr)
  call mpi_bcast(resreduction,1,MPI_DOUBLE_PRECISION,master_rank,MPI_COMM_WORLD,ierr)
  solver_param_out%solvertype = solvertype
  solver_param_out%maxiter = maxiter
  solver_param_out%resreduction = resreduction
end subroutine read_solver_parameters

!==================================================================
! Read grid parameters from namelist file
!==================================================================
subroutine read_grid_parameters(filename,grid_param)
  use parameters
  use datatypes
  use communication
  use messages
  use mpi
  implicit none
  character(*), intent(in) :: filename
  type(grid_parameters), intent(out) :: grid_param
  ! Grid parameters
  integer :: n, nz
  real(kind=rl) :: L, H
  integer :: vertbc
  logical :: graded
  integer, parameter :: file_id = 16
  integer :: ierr
  namelist /parameters_grid/ n, nz, L, H, vertbc, graded
  if (i_am_master_mpi) then
    open(file_id,file=filename)
    read(file_id,NML=parameters_grid)
    close(file_id)
    write(STDOUT,NML=parameters_grid)
    write(STDOUT,'("---------------------------------------------- ")')
    write(STDOUT,'("Grid parameters")')
    write(STDOUT,'("    n      = ",I15)') n
    write(STDOUT,'("    nz     = ",I15)') nz
    write(STDOUT,'("    L      = ",F8.4)') L
    write(STDOUT,'("    H      = ",F8.4)') H
    if (vertbc == VERTBC_DIRICHLET) then
      write(STDOUT,'("    vertbc = DIRICHLET")')
    else if (vertbc == VERTBC_NEUMANN) then
      write(STDOUT,'("    vertbc = NEUMANN")')
    else
      vertbc = -1
    end if
    write(STDOUT,'("    graded =",L3)') graded
    write(STDOUT,'("---------------------------------------------- ")')
    write(STDOUT,'("")')
  end if
  call mpi_bcast(n,1,MPI_INTEGER,master_rank,MPI_COMM_WORLD,ierr)
  call mpi_bcast(nz,1,MPI_INTEGER,master_rank,MPI_COMM_WORLD,ierr)
  call mpi_bcast(L,1,MPI_DOUBLE_PRECISION,master_rank,MPI_COMM_WORLD,ierr)
  call mpi_bcast(H,1,MPI_DOUBLE_PRECISION,master_rank,MPI_COMM_WORLD,ierr)
  call mpi_bcast(vertbc,1,MPI_INTEGER,master_rank,MPI_COMM_WORLD,ierr)
  call mpi_bcast(graded,1,MPI_LOGICAL,master_rank,MPI_COMM_WORLD,ierr)
  grid_param%n = n
  grid_param%nz = nz
  grid_param%L = L
  grid_param%H = H
  grid_param%vertbc = vertbc
  grid_param%graded = graded
  if (vertbc == -1) then
    call fatalerror("vertbc has to be either 1 (Dirichlet) or 2 (Neumann)")
  end if
end subroutine read_grid_parameters

!==================================================================
! Read parallel communication parameters from namelist file
!==================================================================
subroutine read_comm_parameters(filename,comm_param)
  use parameters
  use communication
  use messages
  use mpi
  implicit none
  character(*), intent(in) :: filename
  type(comm_parameters), intent(out) :: comm_param
  ! Grid parameters
  integer :: halo_size
  integer, parameter :: file_id = 16
  integer :: ierr
  namelist /parameters_communication/ halo_size
  if (i_am_master_mpi) then
    open(file_id,file=filename)
    read(file_id,NML=parameters_communication)
    close(file_id)
    write(STDOUT,NML=parameters_communication)
    write(STDOUT,'("---------------------------------------------- ")')
    write(STDOUT,'("Communication parameters")')
    write(STDOUT,'("    halosize  = ",I3)') halo_size
    write(STDOUT,'("---------------------------------------------- ")')
    write(STDOUT,'("")')
    if ( (halo_size .ne. 1) ) then
      halo_size = -1
    end if
  end if
  call mpi_bcast(halo_size,1,MPI_INTEGER,master_rank,MPI_COMM_WORLD,ierr)
  comm_param%halo_size = halo_size
  if (halo_size == -1) then
    call fatalerror("Halo size has to be 1.")
  end if
end subroutine read_comm_parameters

!==================================================================
! Read model parameters from namelist file
!==================================================================
subroutine read_model_parameters(filename,model_param)
  use parameters
  use discretisation
  use communication
  use messages
  use mpi
  implicit none
  character(*), intent(in) :: filename
  type(model_parameters), intent(out) :: model_param
  real(kind=rl) :: omega2, lambda2, delta
  integer, parameter :: file_id = 16
  integer :: ierr
  namelist /parameters_model/ omega2, lambda2, delta
  if (i_am_master_mpi) then
    open(file_id,file=filename)
    read(file_id,NML=parameters_model)
    close(file_id)
    write(STDOUT,NML=parameters_model)
    write(STDOUT,'("---------------------------------------------- ")')
    write(STDOUT,'("Model parameters")')
    write(STDOUT,'("    omega2  = ",E15.6)') omega2
    write(STDOUT,'("    lambda2 = ",E15.6)') lambda2
    write(STDOUT,'("    delta   = ",E15.6)') delta
    write(STDOUT,'("---------------------------------------------- ")')
    write(STDOUT,'("")')
  end if
  call mpi_bcast(omega2,1,MPI_DOUBLE_PRECISION,master_rank,MPI_COMM_WORLD,ierr)
  call mpi_bcast(lambda2,1,MPI_DOUBLE_PRECISION,master_rank,MPI_COMM_WORLD,ierr)
  call mpi_bcast(delta,1,MPI_DOUBLE_PRECISION,master_rank,MPI_COMM_WORLD,ierr)
  model_param%omega2 = omega2
  model_param%lambda2 = lambda2
  model_param%delta = delta
end subroutine read_model_parameters

!==================================================================
! Read smoother parameters from namelist file
!==================================================================
subroutine read_smoother_parameters(filename,smoother_param)
  use parameters
  use discretisation
  use communication
  use messages
  use mpi
  implicit none
  character(*), intent(in) :: filename
  type(smoother_parameters), intent(out) :: smoother_param
  integer, parameter :: file_id = 16
  integer :: smoother, ordering
  real(kind=rl) :: rho
  integer :: ierr
  namelist /parameters_smoother/ smoother,           &
                                  ordering,           &
                                  rho
  if (i_am_master_mpi) then
    open(file_id,file=filename)
    read(file_id,NML=parameters_smoother)
    close(file_id)
    write(STDOUT,NML=parameters_smoother)

    write(STDOUT,'("---------------------------------------------- ")')
    write(STDOUT,'("Smoother parameters")')
    ! Smoother
    if (smoother == SMOOTHER_LINE_SOR) then
      write(STDOUT,'("    smoother      = LINE_SOR")')
    else if (smoother == SMOOTHER_LINE_SSOR) then
      write(STDOUT,'("    smoother      = LINE_SSOR")')
    else if (smoother == SMOOTHER_LINE_JAC) then
      write(STDOUT,'("    smoother      = LINE_JACOBI")')
    else
      smoother = -1
    end if

    if (ordering == ORDERING_LEX) then
      write(STDOUT,'("    ordering      = LEX")')
    else if (ordering == ORDERING_RB) then
      write(STDOUT,'("    ordering      = RB")')
    else
      ordering = -1
    end if
    write(STDOUT,'("    rho = ",E15.6)') rho
    write(STDOUT,'("---------------------------------------------- ")')
    write(STDOUT,'("")')
  end if
  call mpi_bcast(smoother,1,MPI_INTEGER,master_rank,MPI_COMM_WORLD,ierr)
  call mpi_bcast(ordering,1,MPI_INTEGER,master_rank,MPI_COMM_WORLD,ierr)
  call mpi_bcast(rho,1,MPI_DOUBLE_PRECISION,master_rank,MPI_COMM_WORLD,ierr)
  smoother_param%smoother = smoother
  smoother_param%ordering = ordering
  smoother_param%rho = rho
  if (smoother == -1) then
    call fatalerror('Unknown smoother.')
  end if
  if (ordering == -1) then
    call fatalerror('Unknown ordering.')
  end if

end subroutine read_smoother_parameters

!==================================================================
! Read multigrid parameters from namelist file
!==================================================================
subroutine read_multigrid_parameters(filename,mg_param)
  use parameters
  use multigrid
  use communication
  use messages
  use mpi
  implicit none
  character(*), intent(in) :: filename
  type(mg_parameters), intent(out) :: mg_param
  integer, parameter :: file_id = 16
  integer :: verbose, n_lev, lev_split, n_presmooth, n_postsmooth,    &
             prolongation, restriction, n_coarsegridsmooth, &
             coarsegridsolver
  integer :: ierr
  namelist /parameters_multigrid/ verbose,            &
                                  n_lev,              &
                                  lev_split,          &
                                  n_presmooth,        &
                                  n_postsmooth,       &
                                  n_coarsegridsmooth, &
                                  prolongation,       &
                                  restriction,        &
                                  coarsegridsolver
  if (i_am_master_mpi) then
    open(file_id,file=filename)
    read(file_id,NML=parameters_multigrid)
    close(file_id)
    write(STDOUT,NML=parameters_multigrid)
    write(STDOUT,'("---------------------------------------------- ")')
    write(STDOUT,'("Multigrid parameters")')
    write(STDOUT,'("    verbose      = ",L6)') verbose
    write(STDOUT,'("    levels       = ",I3)') n_lev
    write(STDOUT,'("    splitlevel   = ",I3)') lev_split
    write(STDOUT,'("    n_presmooth  = ",I6)') n_presmooth
    write(STDOUT,'("    n_postsmooth = ",I6)') n_postsmooth
    if (restriction == REST_CELLAVERAGE) then
      write(STDOUT,'("    restriction   = CELLAVERAGE")')
    else
      restriction = -1
    endif
    if (prolongation == PROL_CONSTANT) then
      write(STDOUT,'("    prolongation  = CONSTANT")')
    else if (prolongation == PROL_TRILINEAR) then
#ifdef PIECEWISELINEAR
      write(STDOUT,'("    prolongation  = TRILINEAR (piecewise linear)")')
#else
      write(STDOUT,'("    prolongation  = TRILINEAR (regression plane)")')
#endif
    else
      prolongation = -1
    endif
    if (coarsegridsolver == COARSEGRIDSOLVER_CG) then
      write(STDOUT,'("    coarse solver = CG")')
    else if (coarsegridsolver == COARSEGRIDSOLVER_SMOOTHER) then
      write(STDOUT,'("    coarse solver = SMOOTHER (",I6," iterations)")') &
        n_coarsegridsmooth
    else
      coarsegridsolver = -1
    end if
    write(STDOUT,'("---------------------------------------------- ")')
    write(*,'("")')

  end if
  call mpi_bcast(verbose,1,MPI_INTEGER,master_rank,MPI_COMM_WORLD,ierr)
  call mpi_bcast(n_lev,1,MPI_INTEGER,master_rank,MPI_COMM_WORLD,ierr)
  call mpi_bcast(lev_split,1,MPI_INTEGER,master_rank,MPI_COMM_WORLD,ierr)
  call mpi_bcast(n_presmooth,1,MPI_INTEGER,master_rank,MPI_COMM_WORLD,ierr)
  call mpi_bcast(n_postsmooth,1,MPI_INTEGER,master_rank,MPI_COMM_WORLD,ierr)
  call mpi_bcast(n_coarsegridsmooth,1,MPI_INTEGER,master_rank,MPI_COMM_WORLD, &
                 ierr)
  call mpi_bcast(prolongation,1,MPI_INTEGER,master_rank,MPI_COMM_WORLD,ierr)
  call mpi_bcast(restriction,1,MPI_INTEGER,master_rank,MPI_COMM_WORLD,ierr)
  call mpi_bcast(coarsegridsolver,1,MPI_Integer,master_rank,MPI_COMM_WORLD,ierr)
  mg_param%verbose = verbose
  mg_param%n_lev = n_lev
  mg_param%lev_split = lev_split
  mg_param%n_presmooth = n_presmooth
  mg_param%n_postsmooth = n_postsmooth
  mg_param%n_coarsegridsmooth = n_coarsegridsmooth
  mg_param%prolongation = prolongation
  mg_param%restriction = restriction
  mg_param%coarsegridsolver = coarsegridsolver
  if (restriction == -1) then
    call fatalerror('Unknown restriction.')
  end if
  if (prolongation == -1) then
    call fatalerror('Unknown prolongation.')
  end if
  if (coarsegridsolver == -1) then
    call fatalerror('Unknown coarse grid solver.')
  end if
end subroutine read_multigrid_parameters

!==================================================================
! Read conjugate gradient parameters from namelist file
!==================================================================
subroutine read_conjugategradient_parameters(filename,cg_param)
  use parameters
  use communication
  use conjugategradient
  use communication
  use messages
  use mpi
  implicit none
  character(*), intent(in) :: filename
  type(cg_parameters), intent(out) :: cg_param
  integer, parameter :: file_id = 16
  integer :: verbose, maxiter, n_prec
  real(kind=rl) :: resreduction
  integer :: ierr
  namelist /parameters_conjugategradient/ verbose,      &
                                          maxiter,      &
                                          resreduction, &
                                          n_prec
  if (i_am_master_mpi) then
    open(file_id,file=filename)
    read(file_id,NML=parameters_conjugategradient)
    close(file_id)
    write(STDOUT,NML=parameters_conjugategradient)
    write(STDOUT,'("---------------------------------------------- ")')
    write(STDOUT,'("Conjugate gradient parameters")')
    write(STDOUT,'("    verbose       = ",I6)') verbose
    write(STDOUT,'("    maxiter       = ",I6)') maxiter
    write(STDOUT,'("    resreduction  = ",E15.6)') resreduction
    write(STDOUT,'("    n_prec        = ",I6)') n_prec
    write(STDOUT,'("---------------------------------------------- ")')
    write(STDOUT,'("")')
  end if
  call mpi_bcast(verbose,1,MPI_INTEGER,master_rank,MPI_COMM_WORLD,ierr)
  call mpi_bcast(maxiter,1,MPI_INTEGER,master_rank,MPI_COMM_WORLD,ierr)
  call mpi_bcast(resreduction,1,MPI_DOUBLE_PRECISION,master_rank,MPI_COMM_WORLD,ierr)
  call mpi_bcast(n_prec,1,MPI_INTEGER,master_rank,MPI_COMM_WORLD,ierr)
  cg_param%verbose = verbose
  cg_param%maxiter = maxiter
  cg_param%resreduction = resreduction
  cg_param%n_prec = n_prec
end subroutine read_conjugategradient_parameters

