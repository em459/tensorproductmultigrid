!=== COPYRIGHT AND LICENSE STATEMENT ===
!
!  This file is part of the TensorProductMultigrid code.
!  
!  (c) The copyright relating to this work is owned jointly by the
!  Crown, Met Office and NERC [2014]. However, it has been created
!  with the help of the GungHo Consortium, whose members are identified
!  at https://puma.nerc.ac.uk/trac/GungHo/wiki .
!  
!  Main Developer: Eike Mueller
!  
!  TensorProductMultigrid is free software: you can redistribute it and/or
!  modify it under the terms of the GNU Lesser General Public License as
!  published by the Free Software Foundation, either version 3 of the
!  License, or (at your option) any later version.
!  
!  TensorProductMultigrid is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU Lesser General Public License for more details.
!  
!  You should have received a copy of the GNU Lesser General Public License
!  along with TensorProductMultigrid (see files COPYING and COPYING.LESSER).
!  If not, see <http://www.gnu.org/licenses/>.
!
!=== COPYRIGHT AND LICENSE STATEMENT ===


!==================================================================
!
! Conjugate gradient solver
!
!    Eike Mueller, University of Bath, Feb 2012
!
!==================================================================
module conjugategradient

  use parameters
  use datatypes
  use discretisation
  use messages
  use communication
  use mpi

  implicit none

public::cg_parameters
public::cg_initialise
public::cg_finalise
public::cg_solve

private

  ! --- Conjugate gradient parameters type ---
  type cg_parameters
    ! Verbosity level
    integer :: verbose
    ! Maximal number of iterations
    integer :: maxiter
    ! Required residual reduction
    real(kind=rl) :: resreduction
    ! Smoother iterations in preconditioner
    integer :: n_prec
  end type cg_parameters

! --- Parameters ---
  type(cg_parameters) :: cg_param
  type(grid_parameters) :: grid_param

contains

!==================================================================
! Initialise conjugate gradient module,
!==================================================================
  subroutine cg_initialise(cg_param_in)    &  ! Conjugate gradient
                                           &  ! parameters
    implicit none
    type(cg_parameters), intent(in)    :: cg_param_in

    if (i_am_master_mpi) then
      write(STDOUT,*) '*** Initialising Conjugate gradient ***'
      write(STDOUT,*) ''
    end if
    cg_param = cg_param_in
  end subroutine cg_initialise

!==================================================================
! Finalise conjugate gradient module,
!==================================================================
  subroutine cg_finalise()
    implicit none

    if (i_am_master_mpi) then
      write(STDOUT,*) '*** Finalising Conjugate gradient ***'
      write(STDOUT,*) ''
    end if
  end subroutine cg_finalise

!==================================================================
! Solve A.u = b.
!==================================================================
  subroutine cg_solve(level,m,b,u)
    implicit none
    integer, intent(in)           :: level
    integer, intent(in)           :: m
    type(scalar3d), intent(in)    :: b    ! RHS vector
    type(scalar3d), intent(inout) :: u    ! solution vector
    type(scalar3d)                :: p    ! } Auxilliary vectors
    type(scalar3d)                :: r    ! } Auxilliary vectors
    type(scalar3d)                :: Ap   ! }
    type(scalar3d)                :: z    ! }
    integer                       :: n_lin
    real(kind=rl)                 :: res0, rz, rz_old, res, alpha
    integer :: i
    logical :: solver_converged = .false.
    integer :: n, nz, nlocal, halo_size
    real(kind=rl) :: pAp

    ! Initialise auxiliary fields
    p%grid_param = u%grid_param
    p%ix_min = u%ix_min
    p%ix_max = u%ix_max
    p%iy_min = u%iy_min
    p%iy_max = u%iy_max
    p%icompx_min = u%icompx_min
    p%icompx_max = u%icompx_max
    p%icompy_min = u%icompy_min
    p%icompy_max = u%icompy_max
    p%halo_size = u%halo_size

    r%grid_param = u%grid_param
    r%ix_min = u%ix_min
    r%ix_max = u%ix_max
    r%iy_min = u%iy_min
    r%iy_max = u%iy_max
    r%icompx_min = u%icompx_min
    r%icompx_max = u%icompx_max
    r%icompy_min = u%icompy_min
    r%icompy_max = u%icompy_max
    r%halo_size = u%halo_size

    z%grid_param = u%grid_param
    z%ix_min = u%ix_min
    z%ix_max = u%ix_max
    z%iy_min = u%iy_min
    z%iy_max = u%iy_max
    z%icompx_min = u%icompx_min
    z%icompx_max = u%icompx_max
    z%icompy_min = u%icompy_min
    z%icompy_max = u%icompy_max
    z%halo_size = u%halo_size

    Ap%grid_param = u%grid_param
    Ap%ix_min = u%ix_min
    Ap%ix_max = u%ix_max
    Ap%iy_min = u%iy_min
    Ap%iy_max = u%iy_max
    Ap%icompx_min = u%icompx_min
    Ap%icompx_max = u%icompx_max
    Ap%icompy_min = u%icompy_min
    Ap%icompy_max = u%icompy_max
    Ap%halo_size = u%halo_size

    n = u%ix_max-u%ix_min+1
    nz = u%grid_param%nz

    nlocal = u%ix_max - u%ix_min + 1
    halo_size = u%halo_size

    n_lin = (nlocal+2*halo_size)**2 * (nz+2)

    allocate(r%s(0:nz+1,                     &
             1-halo_size:nlocal+halo_size,   &
             1-halo_size:nlocal+halo_size) )
    allocate(z%s(0:nz+1,                     &
             1-halo_size:nlocal+halo_size,   &
             1-halo_size:nlocal+halo_size) )
    allocate(p%s(0:nz+1,                     &
             1-halo_size:nlocal+halo_size,   &
             1-halo_size:nlocal+halo_size) )
    allocate(Ap%s(0:nz+1,                    &
             1-halo_size:nlocal+halo_size,   &
             1-halo_size:nlocal+halo_size) )
    r%s = 0.0_rl
    z%s = 0.0_rl
    p%s = 0.0_rl
    Ap%s = 0.0_rl

    ! Initialise
    ! r <- b - A.u
    call calculate_residual(level,m,b,u,r)
    ! z <- M^{-1} r
    if (cg_param%n_prec > 0) then
      call smooth(level,m,cg_param%n_prec,DIRECTION_FORWARD,r,z)
      call smooth(level,m,cg_param%n_prec,DIRECTION_BACKWARD,r,z)
    else
      call dcopy(n_lin,r%s,1,z%s,1)
    end if
    ! p <- z
    call dcopy(n_lin,z%s,1,p%s,1)
    ! rz_old = <r,z>
    call scalarprod(m,r,z,rz_old)
    ! res0 <- ||r||
    call scalarprod(m,r,r,res0)
    res0 = dsqrt(res0)
    if (cg_param%verbose > 0) then
      if (i_am_master_mpi) then
        write(STDOUT,'("    *** CG Solver ( ",I10," dof ) ***")') n_lin
        write(STDOUT,'("    <CG> Initial residual ||r_0|| = ",E12.6)') res0
      end if
    endif
    if (res0 > tolerance) then
      do i=1,cg_param%maxiter
        ! Ap <- A.p
        call haloswap(level,m,p)
        call apply(p,Ap)
        ! alpha <- res_old / <p,A.p>
        call scalarprod(m,p,Ap,pAp)
        alpha = rz_old/pAp
        ! x <- x + alpha*p
        call daxpy(n_lin,alpha,p%s,1,u%s,1)
        ! r <- r - alpha*A.p
        call daxpy(n_lin,-alpha,Ap%s,1,r%s,1)
        call scalarprod(m,r,r,res)
        res = dsqrt(res)
        if (cg_param%verbose > 1) then
          if (i_am_master_mpi) then
            write(STDOUT,'("    <CG> Iteration ",I6," ||r|| = ",E12.6)') &
              i, res
          end if
        end if
        if ( (res/res0 < cg_param%resreduction) .or. &
             (res < tolerance ) ) then
          solver_converged = .true.
          exit
        end if
        z%s = 0.0_rl
        ! z <- M^{-1} r
        z%s = 0.0_rl
        if (cg_param%n_prec > 0) then
          call smooth(level,m,cg_param%n_prec,DIRECTION_FORWARD,r,z)
          call smooth(level,m,cg_param%n_prec,DIRECTION_BACKWARD,r,z)
        else
          call dcopy(n_lin,r%s,1,z%s,1)
        end if
        call scalarprod(m,r,z,rz)
        ! p <- res/res_old*p
        call dscal(n_lin,rz/rz_old,p%s,1)
        ! p <- p + z
        call daxpy(n_lin,1.0_rl,z%s,1,p%s,1)
        rz_old = rz
      end do
    else
      res = res0
      solver_converged = .true.
    end if
    if (cg_param%verbose>0) then
      if (solver_converged) then
        if (i_am_master_mpi) then
          write(STDOUT,'("    <CG> Final residual    ||r|| = ",E12.6)') res
          write(STDOUT,'("    <CG> CG solver converged after ",I6," iterations rho_avg = ",F10.6)') i, (res/res0)**(1.0_rl/i)
        end if
      else
        call warning("    <CG> Solver did not converge")
      endif
    end if

    deallocate(r%s)
    deallocate(z%s)
    deallocate(p%s)
    deallocate(Ap%s)
  end subroutine cg_solve

end module conjugategradient

