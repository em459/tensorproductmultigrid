Summary
=======

This repository contains a standalone geoemetric multigrid solver for a highly anisotropic model equation for the pressure correction PDE arising in atmospheric models. The multigrid solver is based on the a tensor-product multigrid idea in[1] . It can be used as a standalone solver or as preconditioner for a CG iteration.

Usage
-----

The source code can be found in the directory 'Source'. To compile, edit the Makefile accordingly. The 'Python' directory contains a set of tools for analysing the output fields. Some documentation (which might be out of date and/or incorrect) is contained in the directory 'Documentation'.

Contact
-------

Eike Mueller <e.mueller@bath.ac.uk>

Copyright and Licensing statement
---------------------------------

(c) The copyright relating to this work is owned jointly by the Crown, Met Office and NERC [2014]. However, it has been created with the help of the GungHo Consortium, whose members are identified at https://puma.nerc.ac.uk/trac/GungHo/wiki .

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License, version 3.0, as published by the Free Software Foundation.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License, version 3.0, for more details.
You should have received a copy of the GNU Lesser General Public License along with this program (see files COPYING and COPYING.LESSER in the main directory). If not, see http://www.gnu.org/licenses/.

References
----------

[1] S. Boerm and R. Hiptmair: "Analysis of tensor product multigrid", Numer. Algorithms, 26/3 (2001), pp. 219-234,
