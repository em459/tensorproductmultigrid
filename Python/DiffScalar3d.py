### COPYRIGHT AND LICENSE STATEMENT ###
#
#  This file is part of the TensorProductMultigrid code.
#  
#  (c) The copyright relating to this work is owned jointly by the
#  Crown, Met Office and NERC [2014]. However, it has been created
#  with the help of the GungHo Consortium, whose members are identified
#  at https://puma.nerc.ac.uk/trac/GungHo/wiki .
#  
#  Main Developer: Eike Mueller
#  
#  TensorProductMultigrid is free software: you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#  
#  TensorProductMultigrid is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.
#  
#  You should have received a copy of the GNU Lesser General Public License
#  along with TensorProductMultigrid (see files COPYING and COPYING.LESSER).
#  If not, see <http://www.gnu.org/licenses/>.
#
### COPYRIGHT AND LICENSE STATEMENT ###


import sys
import math
import numpy as np
from Scalar3d import *

##################################################################
##################################################################

def Main(Filename1,Filename2):
  phi1 = Scalar3d(Filename1)
  phi2 = Scalar3d(Filename2)
  print ''
  print 'L2 norm of first file = '+str(phi1.l2norm())
  print 'L2 norm of second file = '+str(phi2.l2norm())
  print 'L2 norm of difference = '+str(phi1.diffl2norm(phi2))

##################################################################
##################################################################
if (__name__ == '__main__'):
  if (len(sys.argv) != 3) :
    print 'Usage: python '+sys.argv[0]+' <filename1> <filename2>'
    sys.exit(0)
  Filename1 = sys.argv[1]
  Filename2 = sys.argv[2]
  Main(Filename1,Filename2)

