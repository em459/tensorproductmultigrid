### COPYRIGHT AND LICENSE STATEMENT ###
#
#  This file is part of the TensorProductMultigrid code.
#  
#  (c) The copyright relating to this work is owned jointly by the
#  Crown, Met Office and NERC [2014]. However, it has been created
#  with the help of the GungHo Consortium, whose members are identified
#  at https://puma.nerc.ac.uk/trac/GungHo/wiki .
#  
#  Main Developer: Eike Mueller
#  
#  TensorProductMultigrid is free software: you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#  
#  TensorProductMultigrid is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.
#  
#  You should have received a copy of the GNU Lesser General Public License
#  along with TensorProductMultigrid (see files COPYING and COPYING.LESSER).
#  If not, see <http://www.gnu.org/licenses/>.
#
### COPYRIGHT AND LICENSE STATEMENT ###


import sys
import math
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
from matplotlib import pyplot as plt
from Scalar3d import *

##################################################################
# Merge data onto one processor field
##################################################################
def Main(FilenameStem,nproc):
  nproclin = int(math.sqrt(nproc))
  phi_a = []
  vmax = -1.E9
  vmin = +1.E9
  for p in range(nproc):
    Filename = FilenameStem+'_'+('%010d' % nproc)+'_'+('%010d' % p)+'.dat'
    phi = Scalar3d(Filename)
    phi_a.append(phi)
    if (phi.C.max() > vmax):
      vmax = phi.C.max()
    if (phi.C.min() < vmin):
      vmin = phi.C.min()
  phiRef = phi_a[0]
  n = phiRef.n
  nz = phiRef.nz
  nlocal = n/nproclin
  halosize = phiRef.halosize
  C = np.zeros((n+2*halosize,
               n+2*halosize,
               nz+2))
  for p in range(nproc):
    phi = phi_a[p]
    C[phi.ix_min:phi.ix_max+1,phi.iy_min:phi.iy_max+1,:] = phi.C[1:nlocal+1,1:nlocal+1,:]
  outFilename = FilenameStem+'_MERGED_'+('%010d' % 1)+'_'+('%010d' % 0)+'.dat'
  outFile = open(outFilename,'w')
  print >> outFile, ' # 3d scalar data file'
  print >> outFile, ' # ==================='
  print >> outFile, ' # Data is written as s(iz,iy,ix) '
  print >> outFile, ' # with the leftmost index running fastest'
  print >> outFile, ' n  = '+('%8d' % n)
  print >> outFile, ' nz = '+('%8d' % nz)
  print >> outFile, ' L  = '+('%20.10f' % phiRef.L)
  print >> outFile, ' H  = '+('%20.10f' % phiRef.H)
  print >> outFile, ' ix_min  = '+('%10d' % 1)
  print >> outFile, ' ix_max  = '+('%10d' % n)
  print >> outFile, ' iy_min  = '+('%10d' % 1)
  print >> outFile, ' iy_max  = '+('%10d' % n)
  print >> outFile, ' icompx_min  = '+('%10d' % 1)
  print >> outFile, ' icompx_max  = '+('%10d' % n)
  print >> outFile, ' icompy_min  = '+('%10d' % 1)
  print >> outFile, ' icompy_max  = '+('%10d' % n)
  print >> outFile, ' halosize    = '+('%10d' % halosize)
  for ix in range(n+2*halosize):
    for iy in range(n+2*halosize):
      for iz in range(nz+2):
        print >> outFile, FormatExp(C[ix,iy,iz])
  outFile.close()

def FormatExp(x):
  s = ('%24.15E' % x)
  (mantissa,exponent) = s.split('E')
  xman = float(mantissa)
  xexp = float(exponent)
  if (xman > 1.0):
    xman *= 0.1
    xexp += 1
  s = ('%20.15f' % xman) + 'E'+('%+03d' % xexp)
  return s

##################################################################
##################################################################
if (__name__ == '__main__'):
  if (len(sys.argv) != 3):
    print 'Usage: python '+sys.argv[0]+' <filenamestem> <nproc>'
    sys.exit(0)
  FilenameStem = sys.argv[1]
  nproc = int(sys.argv[2])
  Main(FilenameStem,nproc)

