### COPYRIGHT AND LICENSE STATEMENT ###
#
#  This file is part of the TensorProductMultigrid code.
#  
#  (c) The copyright relating to this work is owned jointly by the
#  Crown, Met Office and NERC [2014]. However, it has been created
#  with the help of the GungHo Consortium, whose members are identified
#  at https://puma.nerc.ac.uk/trac/GungHo/wiki .
#  
#  Main Developer: Eike Mueller
#  
#  TensorProductMultigrid is free software: you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#  
#  TensorProductMultigrid is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.
#  
#  You should have received a copy of the GNU Lesser General Public License
#  along with TensorProductMultigrid (see files COPYING and COPYING.LESSER).
#  If not, see <http://www.gnu.org/licenses/>.
#
### COPYRIGHT AND LICENSE STATEMENT ###


import sys
import math
import numpy as np

##################################################################
#
##################################################################
class Scalar3d:
  def __init__(self,Filename):
    print 'Reading from file \''+Filename+'\''
    File = open(Filename,'r')
    # Skip first for lines
    for i in range(4):
      File.readline()
    # number of horizontal grid cells
    s = File.readline()
    self.n = int(s.split()[2])
    # number of vertical grid cells
    s = File.readline()
    self.nz = int(s.split()[2])
    # horizontal grid size
    s = File.readline()
    self.L = float(s.split()[2])
    # vertical grid size
    s = File.readline()
    self.H = float(s.split()[2])
    # ix_min
    s = File.readline()
    self.ix_min = int(s.split()[2])
    # ix_max
    s = File.readline()
    self.ix_max = int(s.split()[2])
    # iy_min
    s = File.readline()
    self.iy_min = int(s.split()[2])
    # iy_max
    s = File.readline()
    self.iy_max = int(s.split()[2])
    # icompx_min
    s = File.readline()
    self.icompx_min = int(s.split()[2])
    # icompx_max
    s = File.readline()
    self.icompx_max = int(s.split()[2])
    # icompy_min
    s = File.readline()
    self.icompy_min = int(s.split()[2])
    # icompy_max
    s = File.readline()
    self.icompy_max = int(s.split()[2])
    # halo size
    s = File.readline()
    self.halosize = int(s.split()[2])
    print ' n  = '+str(self.n)
    print ' nz = '+str(self.nz)
    print ' L  = '+str(self.L)
    print ' H  = '+str(self.H)
    print ' ix_min  = '+str(self.ix_min)
    print ' ix_max  = '+str(self.ix_max)
    print ' iy_min  = '+str(self.iy_min)
    print ' iy_max  = '+str(self.iy_max)
    print ' icompx_min  = '+str(self.icompx_min)
    print ' icompx_max  = '+str(self.icompx_max)
    print ' icompy_min  = '+str(self.icompy_min)
    print ' icompy_max  = '+str(self.icompy_max)
    print ' halosize    = '+str(self.halosize)
    # Read data
    nlocalx = self.ix_max - self.ix_min + 1
    nlocaly = self.iy_max - self.iy_min + 1
    self.C = np.zeros((nlocalx+2*self.halosize,
                       nlocaly+2*self.halosize,
                       self.nz+2))
    self.X = np.zeros((nlocalx+2*self.halosize+1,
                       nlocaly+2*self.halosize+1))
    self.Y = np.zeros((nlocalx+2*self.halosize+1,
                       nlocaly+2*self.halosize+1))
    for ix in range(nlocalx+2*self.halosize):
      for iy in range(nlocaly+2*self.halosize):
        for iz in range(self.nz+2):
          self.C[ix,iy,iz] = float(File.readline())
    for ix in range(nlocalx+2*self.halosize+1):
      for iy in range(nlocaly+2*self.halosize+1):
        self.X[ix,iy] = self.L/self.n*(ix+self.ix_min-1.-self.halosize)
        self.Y[ix,iy] = self.L/self.n*(iy+self.iy_min-1.-self.halosize)
    print ' max '+str(self.C.max())
    print ' min '+str(self.C.min())
    File.close()

##################################################################
# Calculate L2 norm
##################################################################
  def l2norm(self):
    nlocalx = self.ix_max - self.ix_min + 1
    nlocaly = self.iy_max - self.iy_min + 1
    nrm = 0.0
    for ix in range(nlocalx+2*self.halosize):
      for iy in range(nlocaly+2*self.halosize):
        for iz in range(self.nz+2):
          nrm += self.C[ix,iy,iz]**2
    return math.sqrt(nrm)

##################################################################
# Calculate L2 norm of self-other
##################################################################
  def diffl2norm(self,other):
    Tolerance = 1.0E-9
    assert(self.n==other.n)
    assert(self.nz==other.nz)
    assert(abs(self.L-other.L)<Tolerance)
    assert(abs(self.H-other.H)<Tolerance)
    assert(self.ix_min==other.ix_min)
    assert(self.ix_max==other.ix_max)
    assert(self.iy_min==other.iy_min)
    assert(self.iy_max==other.iy_max)
    assert(self.icompx_min==other.icompx_min)
    assert(self.icompx_max==other.icompx_max)
    assert(self.icompy_min==other.icompy_min)
    assert(self.icompy_max==other.icompy_max)
    assert(self.halosize==other.halosize)
    nlocalx = self.ix_max - self.ix_min + 1
    nlocaly = self.iy_max - self.iy_min + 1
    nrm = 0.0
    for ix in range(nlocalx+2*self.halosize):
      for iy in range(nlocaly+2*self.halosize):
        for iz in range(self.nz+2):
          nrm += (self.C[ix,iy,iz]-other.C[ix,iy,iz])**2
    return math.sqrt(nrm)

##################################################################
#  Plot a specified vertical level from a scalar3d data field
##################################################################
  def PlotLevel(self,Level,showhalo=True):
    C = self.C[:,:,Level]
    ax = plt.gca()
    if (showhalo):
      dx = self.L/self.n*self.halosize
      dy = self.L/self.n*self.halosize
    else:
      dx = 0.0
      dy = 0.0
    ax.set_xlim(-dx,self.L+dx)
    ax.set_ylim(self.L+dy,-dy)
    ax.set_aspect('equal')
    p = plt.pcolor(self.X,self.Y,C)
    plt.colorbar(p)

